package scripts.fc.missions.fcromeoandjuliet.tasks;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fcromeoandjuliet.data.QuestSettings;

public class Apothecary extends Task
{	
	private final Positionable APOTHECARY_TILE = new RSTile(3195, 3403, 0);
	private final int DISTANCE_THRESHOLD = 1;
	
	@Override
	public void execute()
	{
		if(Player.getPosition().distanceTo(APOTHECARY_TILE) > DISTANCE_THRESHOLD)
			WebWalking.walkTo(APOTHECARY_TILE);
		else
		{	
			if(new NpcDialogue("Talk-to", "Apothecary", 10, 3).execute() && QuestSettings.APOTHECARY_DIALOGUE_TWO.isValid())
				if(Timing.waitCondition(FCConditions.CLICK_CONTINUE_CONDITION, 6000))
					if(NPCChat.clickContinue(true))
						General.sleep(800, 1800);
		}
	}

	@Override
	public boolean shouldExecute()
	{
		return QuestSettings.APOTHECARY_DIALOGUE_ONE.isValid() || QuestSettings.APOTHECARY_DIALOGUE_TWO.isValid();
	}

	@Override
	public String getStatus()
	{
		return "Apothecary";
	}

}
